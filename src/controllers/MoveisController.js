const mongoose = require("mongoose");

const Moveis = mongoose.model("Movies");

var filterFloat = function (value) {
    if(/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/
      .test(value))
      return Number(value);
  return NaN;
}

module.exports = {
    async listMoveis (req, res) {
        console.log(req.body)
        const movies = await Moveis.find({title : { $regex: req.body.title, $options: 'i' }}, {_id: 0})

        return res.json(movies)
    },

    async movieYearGenre (req, res) {
        const movies = await Moveis.find({title : { $regex: '('+req.body.year+')', $options: 'i' } , genres : { $regex: req.body.genre, $options: 'i' }})

        return res.json(movies)
    }
};
