const mongoose = require("mongoose");

const Ratings = mongoose.model("Ratings");

module.exports = {

    async listaMoveisRated (req, res) {
        
        let data = req.body.rating.toString()
        const movies = await Ratings.aggregate(
            [
                {
                    $lookup: {
                        from: 'movies',
                        localField: 'movieId',
                        foreignField: 'movieId',
                        as: 'movies'
                    }
                },
                {
                    $match: {
                        rating : data
                    }
                },
                {
                    $sort: {
                        movieId : -1
                    }
                }
            ]
        )

        return res.json(movies)
    }
};
