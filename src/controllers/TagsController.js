const mongoose = require("mongoose");

const Tags = mongoose.model("Tags");

module.exports = {
    async index(req, res){
        const { page = 1 } = req.query;
        const tags = await Tags.paginate({}, { page, limit: 200 });

        return res.json(tags)
    },

    async show(req, res){
        const tags = await Tags.findById(req.params.id);

        return res.json(tags);
    },

    async store(req, res){
       const tags = await Tags.create(req.body);

       return res.json(tags);       
    },

    async update(req, res){
        const tags = await Tags.findByIdAndUpdate(req.params.id, req.body, { new:true })

        return res.json(tags);
    },

    async destroy(req, res){
        await Tags.findByIdAndRemove(req.params.id);

        return res.send();
    }
};