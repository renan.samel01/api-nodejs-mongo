const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const MovieSchema = new mongoose.Schema({
    movieId:{
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    genres:{
        type: String,
        required: true,
    },
});

MovieSchema.plugin(mongoosePaginate);

mongoose.model('Movies', MovieSchema);