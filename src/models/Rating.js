const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const RatingSchema = new mongoose.Schema({
    userId:{
        type: String,
        required: true,
    },
    movieId:{
        type: String,
        required: true,
    },
    rating: {
        type: String,
        required: true,
    },
    timestamp:{
        type: String,
        required: true,
    },
});

RatingSchema.plugin(mongoosePaginate);

mongoose.model('Ratings', RatingSchema);