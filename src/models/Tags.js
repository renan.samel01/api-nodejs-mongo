const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const TagSchema = new mongoose.Schema({
    userId:{
        type: String,
        required: true,   
    },
    movieId:{
        type: String,
        required: true,
    },
    tag: {
        type: String,
        required: true,
    },
    timestamp:{
        type: String,
        required: true,
    },
});

TagSchema.plugin(mongoosePaginate);

mongoose.model('Tags', TagSchema);