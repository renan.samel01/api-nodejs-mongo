const express = require('express');
const routes = express.Router();

const tagsController = require('./controllers/TagsController');
const movieController = require('./controllers/MoveisController');
const ratingController = require('./controllers/RatingsController');



routes.get("/tags", tagsController.index);
routes.post("/movie", movieController.listMoveis);
routes.post("/movieRated", ratingController.listaMoveisRated);
routes.post("/movieYearGenre", movieController.movieYearGenre);
routes.get("/tags/:id", tagsController.show);
routes.post("/tags", tagsController.store);
routes.put("/tags/:id", tagsController.update);
routes.delete("/tags/:id", tagsController.destroy);

module.exports = routes;